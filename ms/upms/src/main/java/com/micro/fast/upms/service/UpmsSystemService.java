package com.micro.fast.upms.service;

import com.micro.fast.common.service.SsmService;

/**
 * 系统管理接口
 * @author lsy
 */
public interface UpmsSystemService<T,ID> extends SsmService<T,ID>{

}
